package com.citi.training.employees.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTests {

	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;

	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(-1, "FRANK", 10.0));

		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}

	@Test
	@Transactional
	public void test_deleteSomeEmployee() {
		Employee test = mysqlEmployeeDao.create(new Employee(-1,"Conor", 10000));
		Employee test2 = mysqlEmployeeDao.create(new Employee(-1,"Caolon",10000));
		
		mysqlEmployeeDao.deleteById(test2.getId());
		assertEquals(mysqlEmployeeDao.findAll().size(),1);
		assertThat(test).isEqualToComparingFieldByField(mysqlEmployeeDao.findById(test.getId()));
	}
}
